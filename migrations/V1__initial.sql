-- config
CREATE TABLE config (
    key         TEXT    PRIMARY KEY,
    value       BLOB
);

-- users
CREATE TABLE user (
    id          INTEGER PRIMARY KEY,
    hash        BLOB    NOT NULL UNIQUE,
    pubkey      BLOB    NOT NULL UNIQUE
);

-- all events
CREATE TABLE event (
    id          INTEGER PRIMARY KEY,
    hash        BLOB    NOT NULL UNIQUE,
    author      INTEGER NOT NULL    REFERENCES user (id) ON DELETE RESTRICT,
    signature   BLOB    NOT NULL,
    timestamp   INTEGER NOT NULL,
    hopdist     INTEGER NOT NULL,
    post_id     INTEGER UNIQUE      REFERENCES post (id) ON DELETE CASCADE
);
CREATE INDEX event_idx ON event (timestamp);

-- post events
CREATE TABLE post (
    id          INTEGER PRIMARY KEY,
    parent      BLOB,               -- event.hash for post events
    body        TEXT    NOT NULL
);
