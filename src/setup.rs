use crate::Config;
use crate::Error;
use crate::Result;
use openssl::sha::sha256;
use pqcrypto_dilithium::dilithium3::{keypair, PublicKey, SecretKey};
use pqcrypto_traits::sign::{PublicKey as _, SecretKey as _};
use rusqlite::{params, Connection};
use std::convert::TryInto;

mod embedded {
    use refinery::embed_migrations;
    embed_migrations!();
}

/// Performs program setup, including database tasks
pub fn setup(db_path: String) -> Result<Config> {
    let mut conn = Connection::open(db_path)?;
    conn.pragma_update(None, "foreign_keys", &true)?;
    conn.pragma_update(None, "journal_mode", &"WAL")?;

    // Perform migrations
    embedded::migrations::runner().run(&mut conn)?;

    let initialized: bool = conn.query_row(
        "SELECT EXISTS (SELECT 1 FROM config WHERE key = 'initialized')",
        params![],
        |row| row.get(0),
    )?;

    if initialized {
        regular_setup(conn)
    } else {
        initial_setup(conn)
    }
}

fn regular_setup(conn: Connection) -> Result<Config> {
    let pubkey: Vec<u8> = conn.query_row(
        "SELECT value FROM config WHERE key = 'pubkey'",
        params![],
        |row| row.get(0),
    )?;

    let seckey: Vec<u8> = conn.query_row(
        "SELECT value FROM config WHERE key = 'seckey'",
        params![],
        |row| row.get(0),
    )?;

    let userhash: Vec<u8> = conn.query_row(
        "SELECT value FROM config WHERE key = 'userhash'",
        params![],
        |row| row.get(0),
    )?;

    let pubkey = PublicKey::from_bytes(&pubkey)?;
    let seckey = SecretKey::from_bytes(&seckey)?;
    let userhash = userhash.try_into().map_err(|_| Error::Database)?;

    Ok(Config {
        conn,
        pubkey,
        seckey,
        userhash,
    })
}

fn initial_setup(mut conn: Connection) -> Result<Config> {
    // Generate a keypair
    let (pubkey, seckey) = keypair();

    // Get public and secret key bytes
    let pubkey_bytes = pubkey.as_bytes();
    let seckey_bytes = seckey.as_bytes();
    let userhash = sha256(&pubkey_bytes);

    let tx = conn.transaction()?;

    // indicate completion of initial setup
    tx.execute("INSERT INTO config (key) VALUES ('initialized')", params![])?;

    tx.execute(
        "INSERT INTO config (key, value) \
        VALUES ('pubkey', ?), ('seckey', ?), ('userhash', ?)",
        params![pubkey_bytes, seckey_bytes, userhash.to_vec()],
    )?;

    tx.execute(
        "INSERT INTO user (hash, pubkey) VALUES (?, ?)",
        params![userhash.to_vec(), pubkey_bytes],
    )?;

    tx.commit()?;
    Ok(Config {
        conn,
        pubkey,
        seckey,
        userhash,
    })
}
