use std::error;
use std::fmt;
use std::result;

/// The result type for Flameside functions
pub type Result<T> = result::Result<T, Error>;

/// The error returned from Flameside functions
#[derive(Debug)]
pub enum Error {
    /// Failure in a database operation
    Database,
    /// Failure in a cryptographic operation
    Crypto,
    /// Failure during data serialization
    Serialize,
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::Database => write!(f, "database operation failed"),
            Error::Crypto => write!(f, "cryptographic operation failed"),
            Error::Serialize => write!(f, "data serialization failed"),
        }
    }
}

impl From<rusqlite::Error> for Error {
    fn from(_: rusqlite::Error) -> Self {
        Error::Database
    }
}

impl From<refinery::Error> for Error {
    fn from(_: refinery::Error) -> Self {
        Error::Database
    }
}

impl From<pqcrypto_traits::Error> for Error {
    fn from(_: pqcrypto_traits::Error) -> Self {
        Error::Crypto
    }
}

impl From<bincode::Error> for Error {
    fn from(_: bincode::Error) -> Self {
        Error::Serialize
    }
}
