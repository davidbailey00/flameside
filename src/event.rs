use crate::Config;
use crate::Result;
use openssl::sha::sha256;
use pqcrypto_dilithium::dilithium3::sign;
use pqcrypto_traits::sign::SignedMessage;
use rusqlite::params;
use serde::Serialize;

/// Incoming events from other peers or created from `RawEvent`
pub enum Event {
    User([u8; 32], Vec<u8>),
    Post(Post, EventMeta),
}

/// Wrapper for user-created events with the `UserEvent` trait
pub enum RawEvent {
    Post(Post),
}

/// Metadata for user-created events such as `Post`
pub struct EventMeta {
    hash: [u8; 32],
    author: [u8; 32],
    signature: Vec<u8>,
    hopdist: u32,
}

/// Posts containing a timestamp and body, and optionally a parent
#[derive(Serialize)]
pub struct Post {
    timestamp: i64,
    parent: Option<[u8; 32]>,
    body: String,
}

impl Event {
    pub fn insert(self, config: &mut Config) -> Result<()> {
        match self {
            Event::User(_, _) => todo!(),
            Event::Post(post, meta) => post.insert(meta, config),
        }
    }
}

impl RawEvent {
    pub fn into_event(self, config: &Config) -> Result<Event> {
        match self {
            RawEvent::Post(post) => {
                let meta = post.sign(config)?;
                Ok(Event::Post(post, meta))
            }
        }
    }
}

trait UserEvent {
    fn sign(&self, config: &Config) -> Result<EventMeta>;
    fn insert(self, meta: EventMeta, config: &mut Config) -> Result<()>;
}

impl UserEvent for Post {
    fn sign(&self, config: &Config) -> Result<EventMeta> {
        let encoded = bincode::serialize(self)?;
        let hash = sha256(&encoded);
        let signature = sign(&encoded, &config.seckey);
        let signature = signature.as_bytes().to_vec();

        Ok(EventMeta {
            hash,
            author: config.userhash,
            signature,
            hopdist: 0,
        })
    }

    fn insert(self, meta: EventMeta, config: &mut Config) -> Result<()> {
        let parent = self.parent.map(|hash| hash.to_vec());
        let hash = meta.hash.to_vec();
        let author = meta.author.to_vec();
        let signature = meta.signature;
        let timestamp = self.timestamp;
        let hopdist = meta.hopdist;

        let tx = config.conn.transaction()?;

        tx.execute(
            "INSERT INTO post (parent, body) VALUES (?, ?)",
            params![parent, self.body],
        )?;
        let post_id = tx.last_insert_rowid();

        tx.execute(
            "INSERT INTO event (hash, author, signature, timestamp, hopdist, post_id) \
            VALUES (?, (SELECT id FROM user WHERE hash = ?), ?, ?, ?, ?)",
            params![hash, author, signature, timestamp, hopdist, post_id],
        )?;

        Ok(tx.commit()?)
    }
}
