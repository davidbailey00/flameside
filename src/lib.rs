use pqcrypto_dilithium::dilithium3::{PublicKey, SecretKey};
use rusqlite::Connection;

mod error;
/// Event-related structs and enums
pub mod event;
mod setup;

pub use error::{Error, Result};
pub use setup::setup;

/// User and database configuration
pub struct Config {
    conn: Connection,
    #[allow(dead_code)]
    pubkey: PublicKey,
    seckey: SecretKey,
    userhash: [u8; 32],
}
